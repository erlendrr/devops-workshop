# DAG 1
# Hvis vi kunne gjort iterasjonen på nytt:
## Hva ville vi gjort annerledes?
Ingenting, det gikk veldig bra og vi var veldig effektive.

## Hva ville vi gjort likt?
Alt, vi lærte mye av opplegget synes vi.

# Hva gikk bra?
Mye.

# Hva kan forbedres?
Gruppeinndeling. Gruppeinndeling tok veldig lang tid. var rart at workshopen ikke var obligatorisk for alle da viktigeten av arbeidet på tvers av studieprogrammene ble satt veldig stort fokus på på forhånd. ble skjevfordelt på gruppene av den grunn.

# DAG 2
# Hvis vi kunne gjort iterasjonen på nytt:
## Hva ville vi gjort annerledes?

# Hva gikk bra?
Ganske mye i dag gikk bra, men hadde litt utfordringer, spurte om hjelp ganske mye. Var ganske mye som egentlig skulle funke, men ikke funket, og det 
var litt irriterende (altså gi flere ressurser til VM'en, og koden i oppgave 6)

# Hva kan forbedres?
henvis til tips

# Hvorfor er det viktig at ny funksjonalitet blir testet grundig før lansering?


# Hvorfor er det viktig med enhetstesting?


# Hvordan har samarbeidet mellom driftere og utviklere på gruppen vært?
Ganske bra, men hadde kanskje vært lettere for utvikleren om vi hadde hatt mer enn én.

# Har dere tips til forbedring av workshop?
Henviser til samme spørsmål fra første dag.
Hvis det ikke er obligatorisk for alle kan man ha påmelding på forhånd f.eks., slik at det ikke tar 3 timer å lage grupper. 
Krasjer med andre fag - mer tilrettelagt, spesielt for dataingeniør.
