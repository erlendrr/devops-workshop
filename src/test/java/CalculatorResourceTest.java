import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.calculate(expression));

        expression = " 300 - 99 ";
        assertEquals(201, calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+100";
        assertEquals(400, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals(18, calculatorResource.subtraction(expression));
    }

    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "50*3";
        assertEquals(150, calculatorResource.subtraction(expression));

        expression = "2*3";
        assertEquals(6, calculatorResource.subtraction(expression));
    }

    public void testDivison(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "10/2";
        assertEquals(5, calculatorResource.subtraction(expression));

        expression = "150/3";
        assertEquals(50, calculatorResource.subtraction(expression));
    }
}
